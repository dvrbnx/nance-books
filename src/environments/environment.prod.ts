export const environment = {
  production: true,
  coverStorage: 'https://uuxamqpddbrazazfzdqa.supabase.co/storage/v1/object/public/covers/',
  redirectRoute: 'https://www.projectmadrigal.be/reset-password'
};
