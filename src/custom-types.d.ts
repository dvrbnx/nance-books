interface Window {
    $localize: (messageParts: TemplateStringsArray, ...expressions: any[]) => string;
    locale: string;
}