import { NavigationEnd, Router } from '@angular/router';
import { Observable, Subscription, delay, filter, firstValueFrom, lastValueFrom } from 'rxjs';
import { UserService } from 'src/app/services/session.service';
import { UserInfo } from 'src/app/types/user/session-info.type';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ThemeService } from 'src/app/services/theme.service';
import { LanguageService } from 'src/app/services/language.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    public isCollapsed = true;
    public theme$ = this.themeService.theme$;
    public language$ = this.languageService.currentLanguage$;
    public user$: Observable<UserInfo | null | undefined> = this.sessionService.user$;
    
    private routerSubscription!: Subscription;
    
    @Input() public headerLinks: { label: string, route: string|string[] }[] = [];
    @Input() public mobileLinks: { label: string, route: string|string[] }[] = [];
    @Input() public adminLinks: { label: string, route: string|string[] }[] = [];

    public showNotification: boolean = true;
    private activePath!: string;

    public currentLanguage = window.locale;
    public notificationText: string = $localize`:@@renewingSite:Wij zijn druk bezig met het vernieuwen van de site. Alvast bedankt voor je geduld!`;

    constructor(
        private sessionService: UserService,
        private themeService: ThemeService,
        private languageService: LanguageService,
        private router: Router,
        private searchService: SearchService
    ) {
        this.activePath = router.url;
    }

    public async ngOnInit(): Promise<any> {
        this.routerSubscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                // For smoother navbar UI
                delay(300)
            )
            // any on purpose because of error I don't want to bother with right now
            .subscribe((event: any) => {
                this.activePath = event.url;
            });

        if (localStorage.getItem('seenNewsNotification1'))  {
            this.showNotification = false;
        }
    }

    public isActive(route: string | string[]): boolean {
        const currentUrl = this.activePath.split('?')[0];
        const targetUrl = Array.isArray(route) ? route.join('/') : route;
    
        return currentUrl === targetUrl;
    }

    public onNavItemClick(route: string | string[]): void {
        this.toggleCollapsed(true);
        this.searchService.reset();
        this.router.navigate(Array.isArray(route) ? route : [route]);
    }

    public setTheme(theme: 'light'|'dark'): void {
        this.themeService.setTheme(theme);
    }

    public logout(): void {
        this.sessionService.logout().then(() => location.reload());
    }

    public toggleCollapsed(newValue?: boolean): void {
        this.isCollapsed = newValue ? newValue : !this.isCollapsed;
        this.toggleBodyScroll();
    }

    public toggleBodyScroll(): void {
        if (this.isCollapsed) {
            document.body.classList.remove('sidebar-open');
        } else {
            document.body.classList.add('sidebar-open');
        }
    }
 
    public async toggleLanguage(): Promise<void> {
        const language = await firstValueFrom(this.language$);
        if (language == 'nl') {
            this.languageService.setLanguage('en');
        } else {
            this.languageService.setLanguage('nl');
        }
    }

    public hideNotification(): void {
        localStorage.setItem('seenNewsNotification1', 'true');
        this.showNotification = false;
    }

    public ngOnDestroy(): void {
        this.routerSubscription.unsubscribe();
    }
}
