import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { LanguageService } from '../../services/language.service';

@Pipe({
  name: 'localizedDate',
  pure: false
})
export class LocalizedDatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe, private languageService: LanguageService) {}

  transform(value: any, format: string = 'fullDate'): string | null {
    const locale = this.languageService.getLocale();
    return this.datePipe.transform(value, format, undefined, locale);
  }
}
