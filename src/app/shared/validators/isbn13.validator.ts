import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function isbn13Validator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      // Allow empty input
      return null;
    }
    // Check if the length is exactly 10
    return value.length === 13 ? null : { isbn13Invalid: true };
  };
};
