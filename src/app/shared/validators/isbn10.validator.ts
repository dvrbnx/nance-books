import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function isbn10Validator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      // Allow empty input
      return null;
    }
    // Check if the length is exactly 10
    return value.length === 10 ? null : { isbn10Invalid: true };
  };
};
