import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';

@Component({
	selector: 'app-toasts',
	templateUrl: './toasts-container.component.html',
	host: { class: 'toast-container position-fixed top-0 end-0 p-3', style: 'z-index: 1200' },
})
export class ToastsContainer implements OnInit {
    @ViewChild('successTpl') successToast!: TemplateRef<any>;
    @ViewChild('dangerTpl') errorToast!: TemplateRef<any>;

    constructor(public toastService: ToastService) {}

    public ngOnInit(): void {
        this.toastService.successEvent.subscribe((message: string) => {
            this.toastService.show({
                template: this.successToast,
                context: { $implicit: message },
                classname: 'bg-success text-light',
                delay: 10000
            })
        });

        this.toastService.errorEvent.subscribe((message: string) => {
            this.toastService.show({
                template: this.errorToast,
                context: { $implicit: message },
                classname: 'bg-danger text-light',
                delay: 10000
            })
        });
    }
    // trackToast(index: number, toast: any): any {
    //     return toast; // or toast.id if each toast has an `id` property
    //   }
}