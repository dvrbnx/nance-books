import { Directive, ElementRef, HostListener, Renderer2, AfterViewInit } from '@angular/core';

// PROBABLY DELETE 

@Directive({
    selector: '[pmStickySidebar]'
})
export class StickySidebarDirective implements AfterViewInit {
    private sidebar!: HTMLElement;
    private placeholder!: HTMLElement;
    private container!: HTMLElement;
    private sidebarHeight!: number;
    private containerHeight!: number;
    private containerTop!: number;
    private sidebarTop!: number;

    private observer!: MutationObserver;

    constructor(private el: ElementRef, private renderer: Renderer2) {}

    public ngAfterViewInit(): void {
        this.sidebar = this.el.nativeElement;
        this.sidebarTop = this.sidebar.offsetTop;
        this.container = this.sidebar.parentElement as HTMLElement;

        this.observer = new MutationObserver(() => {
            this.initializeSidebar();
            this.adjustSidebarPosition();
        });

        this.observer.observe((this.sidebar.parentElement as HTMLElement), {
            childList: true,
            subtree: true,
            attributes: true, 
            attributeFilter: ['class', 'style'],
        });
    }

    private initializeSidebar(): void {
        this.sidebarHeight = this.sidebar.offsetHeight;
        this.containerHeight = this.container.offsetHeight;
        this.containerTop = this.container.offsetTop;

        if (!this.placeholder) {
            this.placeholder = this.renderer.createElement('div');
        }

        this.renderer.setStyle(this.placeholder, 'height', `${this.sidebarHeight}px`);
        this.renderer.setStyle(this.placeholder, 'width', '100%');
        this.renderer.setStyle(this.placeholder, 'visibility', 'hidden');
    }

    private adjustSidebarPosition(): void {
        const scrollY = window.scrollY || document.documentElement.scrollTop;
        const containerBottom = this.containerTop + this.containerHeight;
        const sidebarBottom = scrollY + this.sidebarHeight;

        if (sidebarBottom > containerBottom) {
            // Adjust the sidebar position to stay within bounds
            const newTop = containerBottom - this.sidebarHeight - this.containerTop;
            this.renderer.setStyle(this.sidebar, 'position', 'absolute');
            this.renderer.setStyle(this.sidebar, 'top', `${newTop}px`);
        }
    }

    @HostListener('window:scroll', [])
    public onWindowScroll(): void {
        const scrollY = window.scrollY || document.documentElement.scrollTop;
        const containerBottom = this.containerTop + this.containerHeight;
        const sidebarWidth = this.sidebar.offsetWidth;
        const sidebarBottom = scrollY + this.sidebarHeight;

        // User is scrolling, show sidebar at all times but careful for bottom of the screen
        if (scrollY >= this.sidebarTop && scrollY + this.sidebarHeight < containerBottom && sidebarBottom < scrollY) {
            this.renderer.setStyle(this.sidebar, 'position', 'fixed');
            this.renderer.setStyle(this.sidebar, 'top', '0');
            this.renderer.setStyle(this.sidebar, 'width', `${sidebarWidth}px`);

            if (!this.placeholder?.parentNode) {
                this.container.insertBefore(this.placeholder, this.sidebar);
            }
        }
        // Sidebar is about to overflow!
        else if (sidebarBottom >= containerBottom) {
            this.renderer.setStyle(this.sidebar, 'position', 'absolute');
            this.renderer.setStyle(this.sidebar, 'top', `${this.containerHeight - this.sidebarHeight}px`);
            this.renderer.setStyle(this.sidebar, 'width', `${sidebarWidth}px`);

            if (this.placeholder?.parentNode) {
                this.container.removeChild(this.placeholder);
            }
        }
        // Scrolling at the top of the screen
        else {
            this.renderer.setStyle(this.sidebar, 'position', '');
            this.renderer.setStyle(this.sidebar, 'top', '');
            this.renderer.setStyle(this.sidebar, 'width', '');

            if (this.placeholder?.parentNode) {
                this.container.removeChild(this.placeholder);
            }
        }
    }
}
