import { User } from './user.type';

export interface SessionInfo {
    accessToken: string;
    refreshToken: string;
    user: User;
}

export interface UserInfo {
    email: string;
    username?: string;
    profilePicture?: string;
    isAdmin: boolean;
}