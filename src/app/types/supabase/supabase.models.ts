export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[];

export type Database = {
  public: {
    Tables: {
      admin_text: {
        Row: {
          created_at: string;
          description: string | null;
          id: number;
          postAuthor: string | null;
          text: string | null;
          title: string | null;
        };
        Insert: {
          created_at?: string;
          description?: string | null;
          id?: number;
          postAuthor?: string | null;
          text?: string | null;
          title?: string | null;
        };
        Update: {
          created_at?: string;
          description?: string | null;
          id?: number;
          postAuthor?: string | null;
          text?: string | null;
          title?: string | null;
        };
        Relationships: [];
      };
      authors: {
        Row: {
          created: string;
          id: number;
          name: string;
        };
        Insert: {
          created?: string;
          id?: number;
          name: string;
        };
        Update: {
          created?: string;
          id?: number;
          name?: string;
        };
        Relationships: [];
      };
      authors_books: {
        Row: {
          authorId: number;
          bookId: number;
          created: string;
        };
        Insert: {
          authorId: number;
          bookId: number;
          created?: string;
        };
        Update: {
          authorId?: number;
          bookId?: number;
          created?: string;
        };
        Relationships: [
          {
            foreignKeyName: "authors_books_authorId_fkey";
            columns: ["authorId"];
            isOneToOne: false;
            referencedRelation: "authors";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "authors_books_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "authors_books_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "authors_books_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_2";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "authors_books_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_3";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "authors_books_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_4";
            referencedColumns: ["id"];
          },
        ];
      };
      baskets: {
        Row: {
          adminNotes: string | null;
          created_at: string;
          id: number;
          pickupDate: string | null;
          pickupType: number;
          returnDate: string | null;
          status: number;
          userId: string | null;
          userNotes: string | null;
        };
        Insert: {
          adminNotes?: string | null;
          created_at?: string;
          id?: number;
          pickupDate?: string | null;
          pickupType?: number;
          returnDate?: string | null;
          status?: number;
          userId?: string | null;
          userNotes?: string | null;
        };
        Update: {
          adminNotes?: string | null;
          created_at?: string;
          id?: number;
          pickupDate?: string | null;
          pickupType?: number;
          returnDate?: string | null;
          status?: number;
          userId?: string | null;
          userNotes?: string | null;
        };
        Relationships: [];
      };
      bookqueue: {
        Row: {
          element: string | null;
          path: string;
          source: string | null;
          title: string;
        };
        Insert: {
          element?: string | null;
          path: string;
          source?: string | null;
          title: string;
        };
        Update: {
          element?: string | null;
          path?: string;
          source?: string | null;
          title?: string;
        };
        Relationships: [];
      };
      books: {
        Row: {
          created: string;
          description: string | null;
          enableSync: boolean;
          hidden: boolean;
          id: number;
          isbn10: string | null;
          isbn13: string | null;
          pageCount: number | null;
          publishedYear: number | null;
          recommended: boolean;
          subtitle: string | null;
          syncedFrom: string[];
          title: string;
          uuid: string | null;
        };
        Insert: {
          created?: string;
          description?: string | null;
          enableSync?: boolean;
          hidden?: boolean;
          id?: number;
          isbn10?: string | null;
          isbn13?: string | null;
          pageCount?: number | null;
          publishedYear?: number | null;
          recommended?: boolean;
          subtitle?: string | null;
          syncedFrom: string[];
          title: string;
          uuid?: string | null;
        };
        Update: {
          created?: string;
          description?: string | null;
          enableSync?: boolean;
          hidden?: boolean;
          id?: number;
          isbn10?: string | null;
          isbn13?: string | null;
          pageCount?: number | null;
          publishedYear?: number | null;
          recommended?: boolean;
          subtitle?: string | null;
          syncedFrom?: string[];
          title?: string;
          uuid?: string | null;
        };
        Relationships: [];
      };
      books_publishers: {
        Row: {
          bookId: number;
          created: string;
          publisherId: number;
        };
        Insert: {
          bookId: number;
          created?: string;
          publisherId: number;
        };
        Update: {
          bookId?: number;
          created?: string;
          publisherId?: number;
        };
        Relationships: [
          {
            foreignKeyName: "books_publishers_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_publishers_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_publishers_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_2";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_publishers_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_3";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_publishers_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_4";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_publishers_publisherId_fkey";
            columns: ["publisherId"];
            isOneToOne: false;
            referencedRelation: "publishers";
            referencedColumns: ["id"];
          },
        ];
      };
      books_tags: {
        Row: {
          bookId: number;
          created: string;
          id: number;
          tagId: number;
        };
        Insert: {
          bookId: number;
          created?: string;
          id?: number;
          tagId: number;
        };
        Update: {
          bookId?: number;
          created?: string;
          id?: number;
          tagId?: number;
        };
        Relationships: [
          {
            foreignKeyName: "books_tags_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_tags_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_tags_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_2";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_tags_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_3";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_tags_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_4";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "books_tags_tagId_fkey";
            columns: ["tagId"];
            isOneToOne: false;
            referencedRelation: "tags";
            referencedColumns: ["id"];
          },
        ];
      };
      configs: {
        Row: {
          created_at: string;
          hint: string | null;
          id: number;
          value: string | null;
        };
        Insert: {
          created_at?: string;
          hint?: string | null;
          id?: number;
          value?: string | null;
        };
        Update: {
          created_at?: string;
          hint?: string | null;
          id?: number;
          value?: string | null;
        };
        Relationships: [];
      };
      copies: {
        Row: {
          basket_id: number | null;
          bookId: number;
          comment: string | null;
          created_at: string;
          id: number;
          state: number | null;
        };
        Insert: {
          basket_id?: number | null;
          bookId: number;
          comment?: string | null;
          created_at?: string;
          id?: number;
          state?: number | null;
        };
        Update: {
          basket_id?: number | null;
          bookId?: number;
          comment?: string | null;
          created_at?: string;
          id?: number;
          state?: number | null;
        };
        Relationships: [
          {
            foreignKeyName: "copies_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "copies_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "copies_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_2";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "copies_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_3";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "copies_bookId_fkey";
            columns: ["bookId"];
            isOneToOne: false;
            referencedRelation: "books_public_view_4";
            referencedColumns: ["id"];
          },
        ];
      };
      events: {
        Row: {
          created_at: string;
          description: string | null;
          eventDate: string | null;
          eventTime: string | null;
          google_maps_link: string | null;
          id: number;
          image: string | null;
          link: string | null;
          location: string | null;
          title: string | null;
        };
        Insert: {
          created_at?: string;
          description?: string | null;
          eventDate?: string | null;
          eventTime?: string | null;
          google_maps_link?: string | null;
          id?: number;
          image?: string | null;
          link?: string | null;
          location?: string | null;
          title?: string | null;
        };
        Update: {
          created_at?: string;
          description?: string | null;
          eventDate?: string | null;
          eventTime?: string | null;
          google_maps_link?: string | null;
          id?: number;
          image?: string | null;
          link?: string | null;
          location?: string | null;
          title?: string | null;
        };
        Relationships: [];
      };
      faq: {
        Row: {
          answer_html: string | null;
          created_at: string;
          id: number;
          order: number;
          question: string | null;
        };
        Insert: {
          answer_html?: string | null;
          created_at?: string;
          id?: number;
          order: number;
          question?: string | null;
        };
        Update: {
          answer_html?: string | null;
          created_at?: string;
          id?: number;
          order?: number;
          question?: string | null;
        };
        Relationships: [];
      };
      posts: {
        Row: {
          author: string | null;
          created_at: string;
          htmlcontent: string | null;
          htmlcontent_EN: string | null;
          id: number;
          image: string | null;
          published: boolean;
          title: string | null;
          title_EN: string | null;
        };
        Insert: {
          author?: string | null;
          created_at?: string;
          htmlcontent?: string | null;
          htmlcontent_EN?: string | null;
          id?: number;
          image?: string | null;
          published?: boolean;
          title?: string | null;
          title_EN?: string | null;
        };
        Update: {
          author?: string | null;
          created_at?: string;
          htmlcontent?: string | null;
          htmlcontent_EN?: string | null;
          id?: number;
          image?: string | null;
          published?: boolean;
          title?: string | null;
          title_EN?: string | null;
        };
        Relationships: [
          {
            foreignKeyName: "posts_author_fkey";
            columns: ["author"];
            isOneToOne: false;
            referencedRelation: "users";
            referencedColumns: ["uuid"];
          },
        ];
      };
      publishers: {
        Row: {
          created: string;
          id: number;
          name: string | null;
        };
        Insert: {
          created?: string;
          id?: number;
          name?: string | null;
        };
        Update: {
          created?: string;
          id?: number;
          name?: string | null;
        };
        Relationships: [];
      };
      requests: {
        Row: {
          availability: string | null;
          borrowedDate: string | null;
          closed: boolean;
          comments: string | null;
          copyId: number;
          created_at: string;
          guestName: string | null;
          id: number;
          userId: string | null;
        };
        Insert: {
          availability?: string | null;
          borrowedDate?: string | null;
          closed?: boolean;
          comments?: string | null;
          copyId: number;
          created_at?: string;
          guestName?: string | null;
          id?: number;
          userId?: string | null;
        };
        Update: {
          availability?: string | null;
          borrowedDate?: string | null;
          closed?: boolean;
          comments?: string | null;
          copyId?: number;
          created_at?: string;
          guestName?: string | null;
          id?: number;
          userId?: string | null;
        };
        Relationships: [
          {
            foreignKeyName: "requests_copyId_fkey";
            columns: ["copyId"];
            isOneToOne: false;
            referencedRelation: "copies";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "requests_userId_fkey";
            columns: ["userId"];
            isOneToOne: false;
            referencedRelation: "users";
            referencedColumns: ["uuid"];
          },
        ];
      };
      tags: {
        Row: {
          created: string;
          displayName: string | null;
          displayName_EN: string | null;
          id: number;
          isCategory: boolean;
          isTw: boolean;
          name: string | null;
          showInFrontend: boolean;
        };
        Insert: {
          created?: string;
          displayName?: string | null;
          displayName_EN?: string | null;
          id?: number;
          isCategory?: boolean;
          isTw?: boolean;
          name?: string | null;
          showInFrontend?: boolean;
        };
        Update: {
          created?: string;
          displayName?: string | null;
          displayName_EN?: string | null;
          id?: number;
          isCategory?: boolean;
          isTw?: boolean;
          name?: string | null;
          showInFrontend?: boolean;
        };
        Relationships: [];
      };
      users: {
        Row: {
          created_at: string;
          email: string | null;
          isAdmin: boolean | null;
          name: string | null;
          phone: string | null;
          pronouns: string | null;
          uuid: string;
        };
        Insert: {
          created_at?: string;
          email?: string | null;
          isAdmin?: boolean | null;
          name?: string | null;
          phone?: string | null;
          pronouns?: string | null;
          uuid: string;
        };
        Update: {
          created_at?: string;
          email?: string | null;
          isAdmin?: boolean | null;
          name?: string | null;
          phone?: string | null;
          pronouns?: string | null;
          uuid?: string;
        };
        Relationships: [];
      };
    };
    Views: {
      books_public_view: {
        Row: {
          authors: string | null;
          available: boolean | null;
          categories: string[] | null;
          description: string | null;
          id: number | null;
          isbn10: string | null;
          isbn13: string | null;
          pageCount: number | null;
          publishedYear: number | null;
          publishers: string | null;
          recommended: boolean | null;
          subtitle: string | null;
          themes: string[] | null;
          title: string | null;
          uuid: string | null;
        };
        Relationships: [];
      };
      books_public_view_2: {
        Row: {
          authors: string | null;
          available: boolean | null;
          categories: string[] | null;
          categories_en: string[] | null;
          description: string | null;
          id: number | null;
          isbn10: string | null;
          isbn13: string | null;
          pageCount: number | null;
          publishedYear: number | null;
          publishers: string | null;
          recommended: boolean | null;
          subtitle: string | null;
          themes: string[] | null;
          themes_en: string[] | null;
          title: string | null;
          uuid: string | null;
        };
        Relationships: [];
      };
      books_public_view_3: {
        Row: {
          authors: string | null;
          available: boolean | null;
          categories: string[] | null;
          categories_en: string[] | null;
          description: string | null;
          id: number | null;
          isbn10: string | null;
          isbn13: string | null;
          pageCount: number | null;
          publishedYear: number | null;
          publishers: string | null;
          recommended: boolean | null;
          subtitle: string | null;
          themes: string[] | null;
          themes_en: string[] | null;
          title: string | null;
          uuid: string | null;
        };
        Relationships: [];
      };
      books_public_view_4: {
        Row: {
          authors: string | null;
          available: boolean | null;
          categories: string[] | null;
          categories_en: string[] | null;
          created: string | null;
          description: string | null;
          id: number | null;
          isbn10: string | null;
          isbn13: string | null;
          pageCount: number | null;
          publishedYear: number | null;
          publishers: string | null;
          recommended: boolean | null;
          subtitle: string | null;
          themes: string[] | null;
          themes_en: string[] | null;
          title: string | null;
          uuid: string | null;
        };
        Relationships: [];
      };
      view_posts: {
        Row: {
          author: string | null;
          created_at: string | null;
          htmlcontent: string | null;
          htmlcontent_EN: string | null;
          id: number | null;
          image: string | null;
          pronouns: string | null;
          title: string | null;
          title_EN: string | null;
        };
        Relationships: [];
      };
      view_posts_admin: {
        Row: {
          author: string | null;
          created_at: string | null;
          htmlcontent: string | null;
          htmlcontent_EN: string | null;
          id: number | null;
          image: string | null;
          pronouns: string | null;
          published: boolean | null;
          title: string | null;
          title_EN: string | null;
        };
        Relationships: [];
      };
    };
    Functions: {
      find_books:
        | {
            Args: {
              search_term: string;
            };
            Returns: {
              bookid: number;
              title: string;
              authorid: number;
              author: string;
              tagid: number;
              tag: string;
            }[];
          }
        | {
            Args: {
              search_term: string;
              author_ids: number[];
              tag_ids: number[];
              publisher_ids: number[];
            };
            Returns: {
              id: number;
            }[];
          };
      find_books_2: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
        }[];
      };
      find_books_2_total_count: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
        }[];
      };
      find_books_3: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
        }[];
      };
      find_books_3_count: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: number;
      };
      find_books_4: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
        }[];
      };
      find_books_4_count: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: number;
      };
      find_books_5: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
          test: number;
        }[];
      };
      find_books_6: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
          test: number;
        }[];
      };
      find_books_7: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
          match: number;
        }[];
      };
      find_books_7_count: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: number;
      };
      find_books_total_count: {
        Args: {
          search_term: string;
          author_ids: number[];
          tag_ids: number[];
          publisher_ids: number[];
        };
        Returns: {
          id: number;
        }[];
      };
      get_tag_counts: {
        Args: {
          tag_ids: number[];
        };
        Returns: {
          id: number;
          displayName: string;
          displayName_EN: string;
          isCategory: boolean;
          book_count: number;
        }[];
      };
      get_tags: {
        Args: {
          tag_ids: number[];
        };
        Returns: {
          id: number;
          displayName: string;
          displayName_EN: string;
          isCategory: boolean;
          book_count: number;
        }[];
      };
      get_tags_2: {
        Args: {
          tag_ids: number[];
          search_term: string;
        };
        Returns: {
          id: number;
          displayName: string;
          displayName_EN: string;
          isCategory: boolean;
          book_count: number;
        }[];
      };
      get_tags_7: {
        Args: {
          tag_ids: number[];
          search_term: string;
        };
        Returns: {
          id: number;
          displayName: string;
          displayName_EN: string;
          isCategory: boolean;
          book_count: number;
        }[];
      };
    };
    Enums: {
      [_ in never]: never;
    };
    CompositeTypes: {
      [_ in never]: never;
    };
  };
};

type PublicSchema = Database[Extract<keyof Database, "public">];

export type Tables<
  PublicTableNameOrOptions extends
    | keyof (PublicSchema["Tables"] & PublicSchema["Views"])
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
        Database[PublicTableNameOrOptions["schema"]]["Views"])
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
      Database[PublicTableNameOrOptions["schema"]]["Views"])[TableName] extends {
      Row: infer R;
    }
    ? R
    : never
  : PublicTableNameOrOptions extends keyof (PublicSchema["Tables"] &
        PublicSchema["Views"])
    ? (PublicSchema["Tables"] &
        PublicSchema["Views"])[PublicTableNameOrOptions] extends {
        Row: infer R;
      }
      ? R
      : never
    : never;

export type TablesInsert<
  PublicTableNameOrOptions extends
    | keyof PublicSchema["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Insert: infer I;
    }
    ? I
    : never
  : PublicTableNameOrOptions extends keyof PublicSchema["Tables"]
    ? PublicSchema["Tables"][PublicTableNameOrOptions] extends {
        Insert: infer I;
      }
      ? I
      : never
    : never;

export type TablesUpdate<
  PublicTableNameOrOptions extends
    | keyof PublicSchema["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Update: infer U;
    }
    ? U
    : never
  : PublicTableNameOrOptions extends keyof PublicSchema["Tables"]
    ? PublicSchema["Tables"][PublicTableNameOrOptions] extends {
        Update: infer U;
      }
      ? U
      : never
    : never;

export type Enums<
  PublicEnumNameOrOptions extends
    | keyof PublicSchema["Enums"]
    | { schema: keyof Database },
  EnumName extends PublicEnumNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicEnumNameOrOptions["schema"]]["Enums"]
    : never = never,
> = PublicEnumNameOrOptions extends { schema: keyof Database }
  ? Database[PublicEnumNameOrOptions["schema"]]["Enums"][EnumName]
  : PublicEnumNameOrOptions extends keyof PublicSchema["Enums"]
    ? PublicSchema["Enums"][PublicEnumNameOrOptions]
    : never;

export type CompositeTypes<
  PublicCompositeTypeNameOrOptions extends
    | keyof PublicSchema["CompositeTypes"]
    | { schema: keyof Database },
  CompositeTypeName extends PublicCompositeTypeNameOrOptions extends {
    schema: keyof Database;
  }
    ? keyof Database[PublicCompositeTypeNameOrOptions["schema"]]["CompositeTypes"]
    : never = never,
> = PublicCompositeTypeNameOrOptions extends { schema: keyof Database }
  ? Database[PublicCompositeTypeNameOrOptions["schema"]]["CompositeTypes"][CompositeTypeName]
  : PublicCompositeTypeNameOrOptions extends keyof PublicSchema["CompositeTypes"]
    ? PublicSchema["CompositeTypes"][PublicCompositeTypeNameOrOptions]
    : never;

// Schema: public
// Tables
export type AdminText = Database["public"]["Tables"]["admin_text"]["Row"];
export type InsertAdminText =
  Database["public"]["Tables"]["admin_text"]["Insert"];
export type UpdateAdminText =
  Database["public"]["Tables"]["admin_text"]["Update"];

export type Author = Database["public"]["Tables"]["authors"]["Row"];
export type InsertAuthor = Database["public"]["Tables"]["authors"]["Insert"];
export type UpdateAuthor = Database["public"]["Tables"]["authors"]["Update"];

export type AuthorBook = Database["public"]["Tables"]["authors_books"]["Row"];
export type InsertAuthorBook =
  Database["public"]["Tables"]["authors_books"]["Insert"];
export type UpdateAuthorBook =
  Database["public"]["Tables"]["authors_books"]["Update"];

export type Basket = Database["public"]["Tables"]["baskets"]["Row"];
export type InsertBasket = Database["public"]["Tables"]["baskets"]["Insert"];
export type UpdateBasket = Database["public"]["Tables"]["baskets"]["Update"];

export type Bookqueue = Database["public"]["Tables"]["bookqueue"]["Row"];
export type InsertBookqueue =
  Database["public"]["Tables"]["bookqueue"]["Insert"];
export type UpdateBookqueue =
  Database["public"]["Tables"]["bookqueue"]["Update"];

export type Book = Database["public"]["Tables"]["books"]["Row"];
export type InsertBook = Database["public"]["Tables"]["books"]["Insert"];
export type UpdateBook = Database["public"]["Tables"]["books"]["Update"];

export type BookPublisher =
  Database["public"]["Tables"]["books_publishers"]["Row"];
export type InsertBookPublisher =
  Database["public"]["Tables"]["books_publishers"]["Insert"];
export type UpdateBookPublisher =
  Database["public"]["Tables"]["books_publishers"]["Update"];

export type BookTag = Database["public"]["Tables"]["books_tags"]["Row"];
export type InsertBookTag =
  Database["public"]["Tables"]["books_tags"]["Insert"];
export type UpdateBookTag =
  Database["public"]["Tables"]["books_tags"]["Update"];

export type Config = Database["public"]["Tables"]["configs"]["Row"];
export type InsertConfig = Database["public"]["Tables"]["configs"]["Insert"];
export type UpdateConfig = Database["public"]["Tables"]["configs"]["Update"];

export type Copy = Database["public"]["Tables"]["copies"]["Row"];
export type InsertCopy = Database["public"]["Tables"]["copies"]["Insert"];
export type UpdateCopy = Database["public"]["Tables"]["copies"]["Update"];

export type Event = Database["public"]["Tables"]["events"]["Row"];
export type InsertEvent = Database["public"]["Tables"]["events"]["Insert"];
export type UpdateEvent = Database["public"]["Tables"]["events"]["Update"];

export type Faq = Database["public"]["Tables"]["faq"]["Row"];
export type InsertFaq = Database["public"]["Tables"]["faq"]["Insert"];
export type UpdateFaq = Database["public"]["Tables"]["faq"]["Update"];

export type Post = Database["public"]["Tables"]["posts"]["Row"];
export type InsertPost = Database["public"]["Tables"]["posts"]["Insert"];
export type UpdatePost = Database["public"]["Tables"]["posts"]["Update"];

export type Publisher = Database["public"]["Tables"]["publishers"]["Row"];
export type InsertPublisher =
  Database["public"]["Tables"]["publishers"]["Insert"];
export type UpdatePublisher =
  Database["public"]["Tables"]["publishers"]["Update"];

export type Request = Database["public"]["Tables"]["requests"]["Row"];
export type InsertRequest = Database["public"]["Tables"]["requests"]["Insert"];
export type UpdateRequest = Database["public"]["Tables"]["requests"]["Update"];

export type Tag = Database["public"]["Tables"]["tags"]["Row"];
export type InsertTag = Database["public"]["Tables"]["tags"]["Insert"];
export type UpdateTag = Database["public"]["Tables"]["tags"]["Update"];

export type User = Database["public"]["Tables"]["users"]["Row"];
export type InsertUser = Database["public"]["Tables"]["users"]["Insert"];
export type UpdateUser = Database["public"]["Tables"]["users"]["Update"];

// Views
export type BookPublicView =
  Database["public"]["Views"]["books_public_view"]["Row"];

export type BookPublicView2 =
  Database["public"]["Views"]["books_public_view_2"]["Row"];

export type BookPublicView3 =
  Database["public"]["Views"]["books_public_view_3"]["Row"];

export type BookPublicView4 =
  Database["public"]["Views"]["books_public_view_4"]["Row"];

export type ViewPost = Database["public"]["Views"]["view_posts"]["Row"];

export type ViewPostAdmin =
  Database["public"]["Views"]["view_posts_admin"]["Row"];

// Functions
export type ArgsFindBook =
  Database["public"]["Functions"]["find_books"]["Args"];
export type ReturnTypeFindBook =
  Database["public"]["Functions"]["find_books"]["Returns"];

export type ArgsFindBook2 =
  Database["public"]["Functions"]["find_books_2"]["Args"];
export type ReturnTypeFindBook2 =
  Database["public"]["Functions"]["find_books_2"]["Returns"];

export type ArgsFindBook2TotalCount =
  Database["public"]["Functions"]["find_books_2_total_count"]["Args"];
export type ReturnTypeFindBook2TotalCount =
  Database["public"]["Functions"]["find_books_2_total_count"]["Returns"];

export type ArgsFindBook3 =
  Database["public"]["Functions"]["find_books_3"]["Args"];
export type ReturnTypeFindBook3 =
  Database["public"]["Functions"]["find_books_3"]["Returns"];

export type ArgsFindBook3Count =
  Database["public"]["Functions"]["find_books_3_count"]["Args"];
export type ReturnTypeFindBook3Count =
  Database["public"]["Functions"]["find_books_3_count"]["Returns"];

export type ArgsFindBook4 =
  Database["public"]["Functions"]["find_books_4"]["Args"];
export type ReturnTypeFindBook4 =
  Database["public"]["Functions"]["find_books_4"]["Returns"];

export type ArgsFindBook4Count =
  Database["public"]["Functions"]["find_books_4_count"]["Args"];
export type ReturnTypeFindBook4Count =
  Database["public"]["Functions"]["find_books_4_count"]["Returns"];

export type ArgsFindBook5 =
  Database["public"]["Functions"]["find_books_5"]["Args"];
export type ReturnTypeFindBook5 =
  Database["public"]["Functions"]["find_books_5"]["Returns"];

export type ArgsFindBook6 =
  Database["public"]["Functions"]["find_books_6"]["Args"];
export type ReturnTypeFindBook6 =
  Database["public"]["Functions"]["find_books_6"]["Returns"];

export type ArgsFindBook7 =
  Database["public"]["Functions"]["find_books_7"]["Args"];
export type ReturnTypeFindBook7 =
  Database["public"]["Functions"]["find_books_7"]["Returns"];

export type ArgsFindBook7Count =
  Database["public"]["Functions"]["find_books_7_count"]["Args"];
export type ReturnTypeFindBook7Count =
  Database["public"]["Functions"]["find_books_7_count"]["Returns"];

export type ArgsFindBookTotalCount =
  Database["public"]["Functions"]["find_books_total_count"]["Args"];
export type ReturnTypeFindBookTotalCount =
  Database["public"]["Functions"]["find_books_total_count"]["Returns"];

export type ArgsGetTagCount =
  Database["public"]["Functions"]["get_tag_counts"]["Args"];
export type ReturnTypeGetTagCount =
  Database["public"]["Functions"]["get_tag_counts"]["Returns"];

export type ArgsGetTag = Database["public"]["Functions"]["get_tags"]["Args"];
export type ReturnTypeGetTag =
  Database["public"]["Functions"]["get_tags"]["Returns"];

export type ArgsGetTag2 = Database["public"]["Functions"]["get_tags_2"]["Args"];
export type ReturnTypeGetTag2 =
  Database["public"]["Functions"]["get_tags_2"]["Returns"];

export type ArgsGetTag7 = Database["public"]["Functions"]["get_tags_7"]["Args"];
export type ReturnTypeGetTag7 =
  Database["public"]["Functions"]["get_tags_7"]["Returns"];
