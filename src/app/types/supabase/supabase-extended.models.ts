import { Book, Tag, Author, Publisher, Copy, User } from "./supabase.models"

export type BookWithRelations = Book & {
    tags: Tag[];
    authors: Author[];
    publishers: Publisher[];
    copies: Copy[];
}

export type RequestWithRelations = Request & {
    users: User[];
    copies: Copy[];
}