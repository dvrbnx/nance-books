export enum State {
    available = 1,

    // deprecated -- should check basketId!
    reserved = 2,
    // deprecated -- should check basketId!
    borrowed = 3,

    damaged = 4,
    lost = 5,
    viewOnly = 6
}
