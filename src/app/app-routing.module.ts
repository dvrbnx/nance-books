import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

// Guards
import { IsAuthorizedGuard } from './guards/authorized.guard';
import { IsLoggedInGuard } from './guards/logged-in.guard';

// Layouts & Main
import { MainComponent } from './main.component';
import { ClientLayoutComponent } from './layouts/client.layout';

// Pages
import { HomeComponent } from './pages/client/home/home.component';
import { AboutComponent } from './pages/client/about/about.component';
import { BooksComponent } from './pages/client/books/books.component';
import { BookDetailComponent } from './pages/client/book-detail/book-detail.component';
import { BookDetailEditorComponent } from './pages/admin/book-detail-edit/book-detail-edit.component';
import { BookRequestComponent } from './pages/client/book-request/book-request.component';
import { BookRequestAdminComponent } from './pages/client/book-request-admin/book-request.admin.component';
import { ManagementComponent } from './pages/admin/management/management.component';
import { ReservationsComponent } from './pages/client/reservations/reservations.component';
import { ReservationsStep2Component } from './pages/client/reservations/step2/reservations-step2.component';

// Auth
import { LoginComponent } from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { RegisterComponent } from './pages/register/register.component';

// Resolvers
import { AuthorsResolver } from './resolvers/authors.resolver';
import { BookResolver } from './resolvers/book.resolver';
import { BookAdminResolver } from './resolvers/book.admin';
import { TagsResolver } from './resolvers/tags.resolver';
import { EditBlogComponent } from './pages/admin/management/blog-management/edit-blog/edit-blog.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      /**
       * -------------------
       * DUTCH ROUTES
       * -------------------
       */
      {
        path: '',
        component: ClientLayoutComponent,
        children: [
          // Home
          {
            path: '',
            component: HomeComponent,
            pathMatch: 'full',
          },
          // About
          {
            path: 'over-project-madrigal',
            component: AboutComponent,
          },
          {
            path: 'about-project-madrigal',
            redirectTo: 'over-project-madrigal',
          },

          {
            path: 'beheer',
            canActivate: [IsAuthorizedGuard],
            children: [
              {
                path: 'boeken',
                component: ManagementComponent
              },
              {
                path: 'tags',
                component: ManagementComponent
              },
              {
                path: 'blog',
                children: [
                  {
                    path: 'edit/:bookId',
                    component: EditBlogComponent
                  },
                  {
                    path: '',
                    pathMatch: 'full',
                    component: ManagementComponent
                  }
                ]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'boeken'
              }
            ]
          },
          // Books (Boeken)
          {
            path: 'boeken',
            children: [
              {
                path: '',
                pathMatch: 'full',
                component: BooksComponent,
              },
              {
                path: 'id/:id',
                children: [
                  {
                    path: '',
                    pathMatch: 'full',
                    component: BookDetailComponent,
                    resolve: { book: BookResolver },
                  },
                  {
                    path: 'bewerken',
                    pathMatch: 'full',
                    component: BookDetailEditorComponent,
                    resolve: {
                      book: BookAdminResolver,
                      tags: TagsResolver,
                      authors: AuthorsResolver,
                    },
                    canActivate: [IsAuthorizedGuard],
                  },
                  {
                    path: 'aanvraag',
                    pathMatch: 'full',
                    component: BookRequestComponent,
                    resolve: { book: BookResolver },
                    canActivate: [IsLoggedInGuard],
                  },
                  {
                    path: 'status-admin',
                    pathMatch: 'full',
                    component: BookRequestAdminComponent,
                    resolve: { book: BookResolver },
                    canActivate: [IsAuthorizedGuard],
                  },
                ],
              },
            ],
          },
          {
            path: 'books',
            redirectTo: 'boeken',
          },

          // Reservations (Reservaties)
          {
            path: 'reservaties',
            children: [
              { path: '1', component: ReservationsComponent },
              { path: '2', component: ReservationsStep2Component },
              { path: '', redirectTo: '1', pathMatch: 'full' },
            ],
          },

          // Authentication
          {
            path: 'login',
            component: LoginComponent,
          },
          {
            path: 'forgot-password',
            component: ForgotPasswordComponent,
          },
          {
            path: 'reset-password',
            component: ResetPasswordComponent,
          },
          {
            path: 'register',
            component: RegisterComponent,
          },
        ],
      },

      /**
       * -------------------
       * ENGLISH ROUTES (prefix: /en)
       * -------------------
       */
      {
        path: 'en',
        component: ClientLayoutComponent,
        children: [
          // Home
          {
            path: '',
            component: HomeComponent,
            pathMatch: 'full',
          },
          // About
          {
            path: 'about-project-madrigal',
            component: AboutComponent,
          },
          {
            path: 'over-project-madrigal',
            redirectTo: 'about-project-madrigal',
          },
          // Books
          {
            path: 'books',
            children: [
              {
                path: '',
                pathMatch: 'full',
                component: BooksComponent,
              },
              {
                path: 'id/:id',
                children: [
                  {
                    path: '',
                    pathMatch: 'full',
                    component: BookDetailComponent,
                    resolve: { book: BookResolver },
                  },
                  {
                    path: 'edit',
                    pathMatch: 'full',
                    component: BookDetailEditorComponent,
                    resolve: {
                      book: BookAdminResolver,
                      tags: TagsResolver,
                      authors: AuthorsResolver,
                    },
                    canActivate: [IsAuthorizedGuard],
                  },
                  {
                    path: 'request',
                    pathMatch: 'full',
                    component: BookRequestComponent,
                    resolve: { book: BookResolver },
                    canActivate: [IsLoggedInGuard],
                  },
                  {
                    path: 'status-admin',
                    pathMatch: 'full',
                    component: BookRequestAdminComponent,
                    resolve: { book: BookResolver },
                    canActivate: [IsAuthorizedGuard],
                  },
                  // Optional Dutch-to-English redirect
                  {
                    path: 'bewerken',
                    redirectTo: 'edit',
                  },
                ],
              },
            ],
          },
          {
            path: 'boeken',
            redirectTo: 'books',
          },

          // Reservations
          {
            path: 'reservations',
            children: [
              { path: '1', component: ReservationsComponent },
              { path: '2', component: ReservationsStep2Component },
              { path: '', redirectTo: '1', pathMatch: 'full' },
            ],
          },

          // Authentication
          {
            path: 'login',
            component: LoginComponent,
          },
          {
            path: 'forgot-password',
            component: ForgotPasswordComponent,
          },
          {
            path: 'reset-password',
            component: ResetPasswordComponent,
          },
          {
            path: 'register',
            component: RegisterComponent,
          },
        ],
      },
    ],
  },
];

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always',
  onSameUrlNavigation: 'ignore'
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routingConfiguration)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
