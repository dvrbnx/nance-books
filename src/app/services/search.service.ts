import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, lastValueFrom, map, firstValueFrom, Subject, ReplaySubject, takeLast } from 'rxjs';
import { LibraryService } from './library.service';
import { Params, Router } from '@angular/router';
import { Publisher, Tag } from '../types/supabase/supabase.models';

export interface Filters {
    page?: number,
    tags?: Tag[] | null,
    tagIds?: number[] | null,
    publishers?: Publisher[] | null,
    searchValue?: string | null
}

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    public books$: BehaviorSubject<any> = new BehaviorSubject<any>({});
    public queryParams$: BehaviorSubject<Params> = new BehaviorSubject<Params>({});
    public bookCount$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    public loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    
    public activeFilters$: ReplaySubject<Filters> = new ReplaySubject<Filters>(1);
    public activeTags$: Observable<Tag[] | undefined | null> = this.activeFilters$.pipe(map(filter => filter.tags));
    public activePublishers$: Observable<Publisher[] | undefined | null> = this.activeFilters$.pipe(map(filter => filter.publishers));
    public searchValue$: Observable<string | undefined | null> = this.activeFilters$.pipe(map(filter => filter.searchValue));

    // The first search should not check the active filters
    private isInitialized: boolean = false;
    private lastSelectedFilter: 'new' | 'relevance' | 'title' | undefined = undefined;

    constructor(
        private libraryService: LibraryService,
        private router: Router
    ) {

    }

    public reset(): void {
        const emptyFilter = {
            page: 1,
            searchValue: null,
            tags: null,
            tagIds: null,
            publishers: null
        };
        this.activeFilters$.next(emptyFilter);
        this.filter(emptyFilter);
    }

    public async filter(filter: any, goToBooksPage?: boolean): Promise<void> {
        this.loading$.next(true);

        if (this.isInitialized) {
            const currentFilter = await firstValueFrom(this.activeFilters$);
            filter =  { ...currentFilter, ...filter };
        }
        
        await this.fetchBooks(filter);

        this.setQueryParams(filter, goToBooksPage);

        this.activeFilters$.next(filter);

        this.isInitialized = true;
        this.loading$.next(false);
    }

    public setSorting(sort: 'relevance'|'new'|'title'): void {
        this.lastSelectedFilter = sort;
        this.filter({ page: 1 });
    }

    public async searchText(searchValue?: string, goToBooksPage?: boolean): Promise<void> {
        if (!searchValue) {
            this.reset();
        }
        await this.filter({ searchValue, page: 1 }, goToBooksPage);
    }

    private async fetchBooks(filter: any): Promise<void> {
        const useSort = this.lastSelectedFilter ?? (filter.searchValue ? 'relevance' : 'new');
        const books = await this.libraryService.getBooks(filter, useSort);
        this.books$.next(books);
        this.bookCount$.next(books.totalCount);
    }

    private setQueryParams(filter: any, goToBooksPage?: boolean): void {
        const queryParams = {
            tags: filter.tagIds?.join(','),
            searchValue: filter.searchValue
        };
        this.queryParams$.next(queryParams);
        const navigateTo = goToBooksPage ? ['boeken'] : [];
        this.router.navigate(navigateTo, { queryParams, queryParamsHandling: 'merge' });
    }
}