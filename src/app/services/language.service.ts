import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class LanguageService {
    private currentLanguage = new BehaviorSubject<string>(window.locale);
    currentLanguage$ = this.currentLanguage.asObservable();
    constructor(private router: Router) { }

    public getLocale(): string {
        return this.currentLanguage.getValue();
    }

    setLanguage(language: string) {
        this.currentLanguage.next(language);

        const currentPath = this.router.url;
        const newPath = this.getLocalizedPath(currentPath, language);

        const translationFile = `/assets/i18n/messages${language === 'nl' ? '' : `.${language}`}.json`;

        fetch(translationFile)
            .then((response) => response.json())
            .then((translations) => {
                window.$localize = (messageParts: TemplateStringsArray, ...expressions: any[]) => {
                    const fullMessage = messageParts[0];
                    const match = fullMessage.match(/:@@(.*?):/);
                    const key = match ? match[1] : fullMessage;
                    const translation = translations.translations[key];
                    return translation || key;
                };
                const localeId = language === 'en' ? 'en' : 'nl';
                document.documentElement.lang = localeId;
                (window as any).LOCALE_ID = localeId;

                localStorage.setItem('preferredLanguage', language);
                this.router.navigateByUrl(newPath);
            })
            .catch((err) => console.error(`Failed to load translation file: ${translationFile}`, err));
    }

    private getLocalizedPath(currentPath: string, language: string): string {
        const parts = currentPath.split('/').filter((part) => part !== '');

        // Remove language from path
        if (parts[0] === 'en') {
            parts.shift();
        }

        // Add language to path
        if (language === 'en') {
            parts.unshift(language);
        }

        return '/' + parts.join('/');
    }
}
