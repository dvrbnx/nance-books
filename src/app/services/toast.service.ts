import { EventEmitter, Injectable, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

export interface Toast {
	template: TemplateRef<any>;
    context?: any;
	classname?: string;
	delay?: number;
}

@Injectable({ providedIn: 'root' })
export class ToastService {
	toasts: Toast[] = [];
    successEvent: EventEmitter<string> = new EventEmitter();
    errorEvent: EventEmitter<string> = new EventEmitter();

	public success(message: string): void {
		this.successEvent.emit(message);
	}

	public error(message: string): void {
		this.errorEvent.emit(message);
	}

	public show(toast: Toast): void {
		this.toasts.push(toast);
	}

	public remove(toast: Toast): void {
		this.toasts = this.toasts.filter((t) => t !== toast);
	}

	public clear(): void {
		this.toasts.splice(0, this.toasts.length);
	}
}