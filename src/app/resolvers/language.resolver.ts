// import { Injectable } from '@angular/core';
// import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { LibraryService } from '../services/library.service';

// @Injectable({ providedIn: 'root' })
// export class LanguagesResolver  {
//   constructor(private libraryService: LibraryService) {}

//   resolve = async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Language[]> => {
//     return await this.libraryService.getLanguages();
//   }
// }