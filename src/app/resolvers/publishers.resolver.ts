import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LibraryService } from '../services/library.service';
import { Publisher } from '../types/supabase/supabase.models';

@Injectable({ providedIn: 'root' })
export class PublishersResolver  {
  constructor(private libraryService: LibraryService) {}

  resolve = async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Publisher[]> => {
    return await this.libraryService.getPublishers();
  }
}