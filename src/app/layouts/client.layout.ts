import { Component, OnDestroy, OnInit } from '@angular/core';
import { filter, firstValueFrom, Subscription } from 'rxjs';
import { UserService } from '../services/session.service';
import { Filters, SearchService } from '../services/search.service';
import { NavigationEnd, Router } from '@angular/router';
import { ThemeService } from '../services/theme.service';

@Component({
    templateUrl: './client.layout.html',
    styleUrls: ['./client.layout.scss']
})
export class ClientLayoutComponent implements OnInit, OnDestroy {
    public routes = [
        { label: $localize`:@@menuHome:Home`, route: $localize`:@@menuHomeUrl:/` },
        { label: $localize`:@@menuBooks:Boeken`, route: [$localize`:@@menuBooksUrl:/boeken`] },
        { label: $localize`:@@menuAbout:Over ons`, route: [$localize`:@@menuAboutUrl:/over-project-madrigal`] }
    ];
    public mobileRoutes = [
        // { label: 'Aanmelden', route: ['/login'] },
        // { label: 'Mijn reservaties', route: ['/reservaties'] },
    ]
    public adminRoutes: { label: string, route: string[] }[] = [
        // Don't add them here! Add them in the ngOnInit function
    ];

    public searchValue?: string;
    public isHomePage?: boolean;

    public theme$ = this.themeService.theme$;
    private filterSubscription!: Subscription;

    public typed: string[] = [
        'queer',
        'gay',
        $localize`:@@lesbian:lesbisch`,
        'bi',
        $localize`:@@nonBinary:non-binair`,
        'transgender',
        'agender',
        $localize`:@@intersex:interseks`,
        'ace',
        $localize`:@@genderNotSure:twijfelend`,
        'out of spoons',
        $localize`:@@welcome:welkom`
    ]

    public text = {
        findABook: $localize`:@@findABook:Vind een boek in de bib van Project Madrigal`,
        iAm: $localize`:@@iAm:Ik ben`,
        searchPlaceholder: $localize`:@@searchPlaceholder:Zoek op titel, auteur, isbn...`
    }

    constructor(
        private sessionService: UserService,
        private searchService: SearchService,
        private themeService: ThemeService,
        private router: Router
    ) { }

    public async ngOnInit(): Promise<void> {
        this.isHomePage = this.router.url === '/' || this.router.url === '/en';

        const savedTheme = localStorage.getItem('theme');
        if (!savedTheme || savedTheme == 'null' || (savedTheme !== 'dark' && savedTheme !== 'light')) {
            this.themeService.setTheme('light');
        }
        if (savedTheme) {
            this.themeService.setTheme(savedTheme as ('light' | 'dark') ?? 'light');
        }

        this.router.events
            .pipe(
                filter((event): event is NavigationEnd => event instanceof NavigationEnd)
            )
            .subscribe((event: NavigationEnd) => {
                this.isHomePage = event.urlAfterRedirects === '/';
            });


        this.filterSubscription = this.searchService.activeFilters$.subscribe((filters: Filters) => {
            this.searchValue = filters.searchValue ?? undefined;
        })

        const user = await firstValueFrom(this.sessionService.user$.pipe(filter(user => !!user)));
        if (user?.isAdmin) {
            this.adminRoutes = [
                { label: 'Beheer', route: ['/beheer'] }
            ];
        }

        console.info('Are you a developer, student or hobby coder? Want to gain experience in a volunteer project? Contact us at info@projectmadrigal.be!');
    }

    public onSearch(): void {
        this.searchService.searchText(this.searchValue, true);
    }

    public ngOnDestroy(): void {
        this.filterSubscription.unsubscribe();
    }
}
