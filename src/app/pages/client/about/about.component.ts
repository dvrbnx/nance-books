import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { TextService } from 'src/app/services/text.service';

@Component({
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
    public article?: {
        title: string;
        text: string;
    }

    constructor(
        private textService: TextService,
        private searchService: SearchService
    ) { }

    public async ngOnInit(): Promise<void> {
        this.article = await this.textService.getAboutText();
    }
}
