import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable, Subject, switchMap, takeUntil, withLatestFrom } from 'rxjs';
import { LanguageService } from 'src/app/services/language.service';
import { LibraryService } from 'src/app/services/library.service';
import { SearchService } from 'src/app/services/search.service';
import { UserService } from 'src/app/services/session.service';
import { ThemeService } from 'src/app/services/theme.service';
import { BookWithRelations } from 'src/app/types/supabase/supabase-extended.models';
import { Book, Copy, Publisher, Tag } from 'src/app/types/supabase/supabase.models';

@Component({
    selector: 'app-books-list',
    templateUrl: './books-list.component.html',
    styleUrls: [
        './books-list.component.scss',
        './loading-spinner.scss'
    ]
})
export class BooksListComponent implements OnInit, OnDestroy {
    public books: any[] = [];
    public totalCount: number = 0;
    public basket: any | null;

    // Pagination
    public pageSize: number = 25;
    public currentPage: number = 1;
    private lockScrollLoad = false;

    // UI
    public theme$ = this.themeService.theme$;
    public loading$: Observable<boolean> = this.searchService.loading$;
    public isDesktop: boolean = false;

    // Search
    public queryParams$ = this.searchService.queryParams$;
    public language$ = this.languageService.currentLanguage$;

    private destroyed$: Subject<void> = new Subject();

    @Output() public reloadBooks: EventEmitter<any> = new EventEmitter();

    constructor(
        private libraryService: LibraryService,
        private searchService: SearchService,
        private route: ActivatedRoute,
        private router: Router,
        private breakpointObserver: BreakpointObserver,
        private themeService: ThemeService,
        private userService: UserService,
        private languageService: LanguageService
    ) { }

    public async ngOnInit(): Promise<void> {
        const page = this.getPageQueryParam();
        const searchValue = this.getSearchValueQueryParam();
        const tagIds = this.getTagsQueryParam();

        this.searchService.activeFilters$
            .pipe(
                withLatestFrom(this.searchService.books$),
                withLatestFrom(this.searchService.bookCount$),
                takeUntil(this.destroyed$)
            )
            .subscribe(([[filters, response], count]) => {
                this.totalCount = count;
                if (response.books) {
                    if (filters.page === 1) {
                        this.books = [];
                    }
                    this.books.push(...response.books);
                }
            });
            this.searchService.filter({ page, searchValue, tagIds });

        this.searchService.loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(loading => {
                this.lockScrollLoad = loading;
            })

        this.breakpointObserver.observe([Breakpoints.Large, Breakpoints.XLarge]).subscribe((result) => {
            this.isDesktop = result.matches;
        });

        this.basket = await this.userService.getBasket();
    }

    private getPageQueryParam(): number | null {
        const page = this.route.snapshot.queryParamMap.get('page');
        if (page) {
            return parseInt(page);
        }
        return 1;
    }

    private getTagsQueryParam(): number[] | null {
        const tags = this.route.snapshot.queryParamMap.get('tags');
        if (tags) {
            return tags.split(',').map(tag => parseInt(tag));
        }
        return null;
    }

    private getSearchValueQueryParam(): string | null {
        return this.route.snapshot.queryParamMap.get('searchValue');
    }

    public goToBook(id: number): void {
        this.router.navigate(['boeken', 'id', id]);
    }

    public getImageUrl(name?: string): string {
        return this.libraryService.getBookCoverUrl(name);
    }

    public setPage(pageNumber: string): void {
        const page = parseInt(pageNumber);
        this.searchService.filter({ page });
    }

    public getTags(book: BookWithRelations): Tag[] {
        return book.tags
            .sort((a, b) => {
                // TODO English lang support
                if (a?.displayName == null || b?.displayName == null) {
                    console.error('One of the tags has an empty display name!');
                    return -1;
                }
                if (a.isCategory !== b.isCategory) {
                    return a.isCategory ? -1 : 1;
                }
                return a.displayName.localeCompare(b.displayName);
            });
    }

    public async addBookToReservation(book: any): Promise<void> {
        await this.libraryService.addBookToReservation(book.id);
        this.basket = await this.userService.getBasket();
        book.available = false;
    }

    public getPublishers(publishers: Publisher[]): string[] {
        return publishers.map(p => p.name ?? '').filter(p => p != '') ?? [];
    }

    public ngOnDestroy(): void {
        this.destroyed$.complete();
    }

    public isAvailable(book: BookWithRelations): boolean {
        return book.copies
            .filter((copy: Copy) => copy.state === 1)
            .length > 0
    }

    public userReservedBook(listBook: any): boolean {
        const matches = this.basket?.books?.filter((book: any) => book.id === listBook.id);
        return matches?.length > 0;
    }

    public isCurrentPage(currentPage: number, pageInPagination: string): boolean {
        return parseInt(pageInPagination) === currentPage;
    }


    public loadMoreBooks(): void {
        if (this.lockScrollLoad) {
            return;
        }
        
        this.currentPage++;
        this.searchService.filter({ page: this.currentPage });
    }

    public onScroll(): void {
        if (this.books.length < this.totalCount) {
            this.loadMoreBooks();
        }
    }
}
