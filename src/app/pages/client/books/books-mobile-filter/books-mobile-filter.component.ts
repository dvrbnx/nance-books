import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { LanguageService } from 'src/app/services/language.service';
import { LibraryService } from 'src/app/services/library.service';
import { SearchService } from 'src/app/services/search.service';
import { TagsService } from 'src/app/services/tags.service';
import { Publisher, ReturnTypeGetTag, Tag } from 'src/app/types/supabase/supabase.models';

@Component({
    selector: 'app-books-mobile-filter',
    templateUrl: './books-mobile-filter.component.html',
    styleUrls: ['./books-mobile-filter.component.scss']
})
export class BooksMobileFilterComponent implements OnInit, OnDestroy {

    public allTags!: ReturnTypeGetTag;
    public allCategories!: ReturnTypeGetTag;
    public allThemes!: ReturnTypeGetTag;

    public allPublishers!: Publisher[];

    public filter: any = {};
    public filterForm!: FormGroup;
    
    public filterCollapsed: boolean = true;
    public language$ = this.languageService.currentLanguage$;

    get categoriesFormArray(): FormArray<FormControl<boolean>> {
        return this.filterForm.get('categories') as FormArray<FormControl<boolean>>;
    }
    get themesFormArray(): FormArray<FormControl<boolean>> {
        return this.filterForm.get('themes') as FormArray<FormControl<boolean>>;
    }

    constructor(
        private tagsService: TagsService,
        private libraryService: LibraryService,
        private searchService: SearchService,
        private languageService: LanguageService,
        private fb: FormBuilder
    ) {
    }

    public async ngOnInit(): Promise<void> {

        this.allPublishers = await this.libraryService.getPublishers();
        this.searchService.activeFilters$.subscribe(async (filter) => {
            this.filter = filter;
            if (filter.tagIds) {
                this.allTags = await this.tagsService.getAllTags(filter.tagIds, filter.searchValue ?? undefined, this.languageService.getLocale());
            }
            this.allCategories = this.allTags.filter(tag => tag.isCategory);
            this.allThemes = this.allTags.filter(tag => !tag.isCategory);

            this.filterForm = this.fb.group({
                categories: this.fb.array(
                  this.allCategories.map((tag) => new FormControl(filter.tagIds?.includes(tag.id))) 
                ),
                themes: this.fb.array(
                    this.allThemes.map((tag) => new FormControl(filter.tagIds?.includes(tag.id))) 
                ),
            });
        });

    }

    public submitFilter(): void {
        this.filterCollapsed = true;
        this.onSearch();
    }

    public onSearch(): void {
        this.searchService.filter({
            tagIds: this.getCheckedTags(),
            // publisherIds: this.selectedPublishers?.map(publisher => publisher.id) ?? null,
            page: 1
        });
    }

    public getTagNamesById(ids: number[]): string {
        return this.allTags?.filter(tag => ids.includes(tag.id)).map(tag => tag.displayName).join(', ');
    }

    private getCheckedTags(): any[] {
        const checkedTags: any[] = [];
        this.categoriesFormArray.value.forEach((checked, index) => {
            if (checked) {
                checkedTags.push(this.allCategories[index].id);
            }
        });
        this.themesFormArray.value.forEach((checked, index) => {
            if (checked) {
                checkedTags.push(this.allThemes[index].id);
            }
        });
        return checkedTags;
    }

    public toggleBodyScroll(): void {
        if (this.filterCollapsed) {
            document.body.classList.remove('sidebar-open');
        } else {
            document.body.classList.add('sidebar-open');
        }
    }

    public ngOnDestroy(): void {
        this.searchService.reset();
    }
}
