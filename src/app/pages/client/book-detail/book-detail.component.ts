import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { firstValueFrom, lastValueFrom, Observable } from 'rxjs';
import { UserService } from 'src/app/services/session.service';
import { LibraryService } from 'src/app/services/library.service';
import { Book, BookPublicView2, Copy, Publisher, Tag } from 'src/app/types/supabase/supabase.models';
import { SearchService } from 'src/app/services/search.service';
import { LanguageService } from 'src/app/services/language.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {
  public book!: BookPublicView2;
  public imageUrl: string = '';
  public breadcrumbs!: any[]
  public themes: string[] = [];
  public categories: string[] = [];
  public publishers: string = '';
  public description: string[] = [];

  public user$: Observable<any> = this.sessionService.user$;
  public searchValue: string | undefined = '';
  public language$ = this.languageService.currentLanguage$;
  // public queryParams$ = this.searchService.queryParams$;
  public queryParams: Params = {};

  private get bookTitle() {
    return this.book?.title;
  }

  constructor(
    private libraryService: LibraryService,
    private sessionService: UserService,
    private searchService: SearchService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) { }

  public ngOnInit(): void {
    this.queryParams = this.route.snapshot.queryParams;
    this.book = this.route.snapshot.data['book'];
    this.breadcrumbs = [
      { label: 'Boeken', routerLink: ['../../'] },
      { label: this.bookTitle },
    ];
    if (this.book.uuid) {
      this.imageUrl = this.libraryService.getBookCoverUrl(this.book.uuid);
    }
    this.themes = this.book.themes ?? [];
    this.categories = this.book.categories ?? [];
    this.publishers = this.book.publishers ?? '';
    this.description = this.book.description?.split('\n') ?? [];
  }

  public onSearch(): void {
    this.searchService.searchText(this.searchValue, true);
  }
}
