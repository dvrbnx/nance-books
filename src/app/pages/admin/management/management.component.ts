import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
    selector: 'app-management',
    templateUrl: 'management.component.html',
    styleUrls: [
        'management.component.scss'
    ]
})

export class ManagementComponent implements OnInit {
    public theme$ = this.themeService.theme$;
    public activeTab: string = 'boeken';

    constructor(
        private themeService: ThemeService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    public ngOnInit(): void {
        const path = this.activatedRoute.snapshot.url[0]?.path;
        const parentPath = this.activatedRoute.parent?.snapshot.url[0]?.path;
        const tabs = ['boeken', 'tags', 'blog'];

        let tabToShow = 'boeken';
        if (tabs.includes(path)) {
            tabToShow = path;
        } else if (parentPath && tabs.includes(parentPath)) {
            tabToShow = parentPath;
        }

        this.activeTab = tabToShow ?? 'boeken';
    }

    public setRoute(route: string): void {
        this.router.navigate(['../', route], { relativeTo: this.activatedRoute });
    }
}