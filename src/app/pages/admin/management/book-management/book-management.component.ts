import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LibraryService } from 'src/app/services/library.service';
import { SupabaseService } from 'src/app/services/supabase.service';
import { ThemeService } from 'src/app/services/theme.service';
import { ToastService } from 'src/app/services/toast.service';
import { BookWithRelations } from 'src/app/types/supabase/supabase-extended.models';

@Component({
    selector: 'app-book-management',
    templateUrl: 'book-management.component.html',
    styleUrls: [
        'book-management.component.scss'
    ]
})

export class BookManagementComponent implements OnInit {
    public hiddenBooks: BookWithRelations[] = [];
    public booksWithoutCover: BookWithRelations[] = [];
    public theme$ = this.themeService.theme$;
    
    public bookForm: FormGroup = new FormGroup({});

    constructor(
        private libraryService: LibraryService,
        private supabaseService: SupabaseService,
        private toastService: ToastService,
        private themeService: ThemeService
    ) { }

    public async ngOnInit(): Promise<void> {
        this.getHiddenBooks();
        this.getBooksWithoutCover();
    }

    public getImageUrl(name?: string): string {
        return this.libraryService.getBookCoverUrl(name);
    }

    private async getHiddenBooks(): Promise<void> {
        const { data, error } = await this.supabaseService.client
            .from('books')
            .select('*, authors(*)')
            .eq('hidden', true);
        this.hiddenBooks = data as BookWithRelations[];
    }
    private async getBooksWithoutCover(): Promise<void> {
        const { data, error } = await this.supabaseService.client
            .from('books')
            .select('*, authors(*)')
            .is('uuid', null);
        this.booksWithoutCover = data as BookWithRelations[];
    }
   
    public async makePublic(bookId: number): Promise<void> {
        const { error } = await this.supabaseService.client
            .from('books')
            .update({ hidden: false })
            .eq('id', bookId);

        if (error) {
            this.toastService.error('Er is iets fout gelopen. Check de console logs en geef door aan Darleen');
            console.error(error);
        } else {
            await this.getHiddenBooks();
            this.toastService.success('Het boek staat terug publiek!');
        }
    }
}