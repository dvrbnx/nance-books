import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LibraryService } from 'src/app/services/library.service';
import { SupabaseService } from 'src/app/services/supabase.service';
import { ThemeService } from 'src/app/services/theme.service';
import { ToastService } from 'src/app/services/toast.service';
import { Book, BookPublicView2, Tag } from 'src/app/types/supabase/supabase.models';

@Component({
    selector: 'app-tag-management',
    templateUrl: 'tag-management.component.html',
    styleUrls: [
        'tag-management.component.scss'
    ]
})

export class TagManagementComponent implements OnInit {
    public tags: Tag[] = [];
    public visibleTags: Tag[] = [];
    public activeTabId: number = 9999;

    public formGroupMap: { [key: string]: FormGroup } = {};
    public bookCache: Map<number, BookPublicView2[]> = new Map();

    public theme$ = this.themeService.theme$;

    constructor(
        private supabaseService: SupabaseService,
        private toastService: ToastService,
        private fb: FormBuilder,
        private libraryService: LibraryService,
        private themeService: ThemeService
    ) {

    }

    public async ngOnInit(): Promise<void> {
        const { data, error } = await this.supabaseService.client
            .from('tags')
            .select('*')
            .order('name');

        if (error) {
            this.toastService.error('Er is iets misgelopen. Check de logs en geef door aan Darleen');
            console.error(error);
        }
        if (!data) {
            this.toastService.error('Er is iets misgelopen. Check de logs en geef door aan Darleen');
            console.error('Geen data gevonden');
        }

        this.tags = data as Tag[];

        // If we ever want to add lazy loading shizzle
        this.visibleTags = this.tags;

        this.visibleTags.forEach((tag) => {
            let tagType = '1';
            if (tag.isCategory) {
                tagType = '2';
            } else if (tag.isTw) {
                tagType = '3';
            }
            this.formGroupMap[tag.id] = this.fb.group({
                id: [tag.id],
                name: [tag.name],
                displayName: [tag.displayName],
                displayNameEn: [tag.displayName_EN],
                showInFrontend: [tag.showInFrontend],
                confirmDelete: [''],
                tagType: [tagType]
            });
        });
    }

    public async getBooksForTag(tagId: number): Promise<void> {
        if (this.bookCache.has(tagId)) {
            return;
        }
        const books = await this.getBooks(tagId);
        this.bookCache.set(tagId, books);
    }

    public async onSubmit(tagId: number): Promise<void> {
        if (this.formGroupMap[tagId].valid) {
            const updatedTag = this.formGroupMap[tagId].value;

            if (this.activeTabId === -1) {
                const { data, error: insertError } = await this.supabaseService.client
                    .from('tags')
                    .insert({
                        'name': updatedTag.name,
                        'displayName': updatedTag.displayName,
                        'displayName_EN': updatedTag.displayNameEn,
                        'showInFrontend': updatedTag.showInFrontend,
                        'isCategory': updatedTag.tagType == '2',
                        'isTw': updatedTag.tagType == '3'
                    })
                    .select('*');
                if (insertError) {
                    this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                    console.error(insertError);
                    return;
                }

                const newTag = data[0];
                this.visibleTags.push(newTag);

                let tagType = '1';
                if (newTag.isCategory) {
                    tagType = '2';
                } else if (newTag.isTw) {
                    tagType = '3';
                }
                this.formGroupMap[newTag.id] = this.fb.group({
                    id: [newTag.id],
                    name: [newTag.name],
                    displayName: [newTag.displayName],
                    displayNameEn: [newTag.displayName_EN],
                    showInFrontend: [newTag.showInFrontend],
                    confirmDelete: [''],
                    tagType: [tagType]
                });
                this.formGroupMap[-1].reset();
                this.activeTabId = newTag.id;
                this.toastService.success('Toegevoegd!');
                return;
            }

            // First, check if we need to update an existing tag
            const existingTags = this.tags.filter(tag => tag.displayName === updatedTag.displayName);
            if (existingTags.length > 0 && existingTags[0].id !== updatedTag.id) {
                // Rewire all the books from this tag to the existing one
                const existingTag = existingTags[0];

                const { data, error: selectError } = await this.supabaseService.client
                    .from('books_tags')
                    .select('bookId')
                    .eq('tagId', tagId);

                if (selectError) {
                    this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                    console.error(selectError);
                    return;
                }
                if (!data) {
                    this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                    console.error('Geen data geslecteerd om te rewiren');
                    return;
                }

                const bookIds = data.map(d => d.bookId);
                for (let bookId of bookIds) {
                    const { error: deleteError } = await this.supabaseService.client
                        .from('books_tags')
                        .delete()
                        .eq('tagId', tagId)
                        .eq('bookId', bookId);

                    if (deleteError) {
                        this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                        console.error(deleteError);
                        return;
                    }

                    const { error: addError } = await this.supabaseService.client
                        .from('books_tags')
                        .insert({
                            'bookId': bookId,
                            'tagId': existingTag.id
                        });

                    if (addError && addError.code != '23505') {
                        this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                        console.error(addError);
                        return;
                    }
                }

                // After rewiring, delete the old tag
                await this.deleteTag(tagId, false);
                this.toastService.success('Succesvol opgeslagen :-)');
                return;
            }

            // Else, just update this tag
            const { error } = await this.supabaseService.client
                .from('tags')
                .update({
                    'name': updatedTag.name,
                    'displayName': updatedTag.displayName,
                    'displayName_EN': updatedTag.displayNameEn,
                    'showInFrontend': updatedTag.showInFrontend,
                    'isCategory': updatedTag.tagType == '2',
                    'isTw': updatedTag.tagType == '3'
                })
                .eq('id', tagId);

            if (error) {
                this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
                console.error(error);
            }

            this.visibleTags = this.visibleTags.map(tag =>
                tag.id === tagId ? { ...tag, ...updatedTag } : tag
            );
            this.toastService.success('Succesvol opgeslagen :-)');
        }
    }

    public async getBooks(tagId: number): Promise<BookPublicView2[]> {
        if (this.activeTabId === -1) {
            return Promise.resolve([]);
        }
        if (this.bookCache.has(tagId)) {
            return Promise.resolve(this.bookCache.get(tagId) ?? []);
        }

        const ids = await this.getBookIds(tagId);

        const { data, error } = await this.supabaseService.client
            .from('books_public_view_2')
            .select('*')
            .in('id', ids);

        if (error) {
            this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
            console.error(error);
        }

        this.bookCache.set(tagId, data as BookPublicView2[]);
        return data as BookPublicView2[] ?? [];

    }

    public getImageUrl(name?: string): string {
        return this.libraryService.getBookCoverUrl(name);
    }

    public async getBookIds(tagId: number): Promise<number[]> {
        const { data, error } = await this.supabaseService.client
            .from('books_tags')
            .select('bookId')
            .eq('tagId', tagId);

        if (error) {
            this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
            console.error(error);
        }

        return data?.map(d => d.bookId) ?? [];
    }

    public async deleteTag(tagId: number, showToast: boolean): Promise<void> {
        const { error } = await this.supabaseService.client
            .from('tags')
            .delete()
            .eq('id', tagId);

        if (error) {
            this.toastService.error('Er is iets misgegaan. Check logs en geef door aan IT');
            console.error(error);
        }
        else {
            if (showToast) {
                this.toastService.success('Tag succesvol verwijderd');
            }
            this.visibleTags = this.visibleTags.filter(tag => tag.id !== tagId);
            this.activeTabId = 9999;
        }
    }

    public newTag(): void {
        this.formGroupMap['-1'] = this.fb.group({
            id: [],
            name: [],
            displayName: [],
            displayNameEn: [],
            showInFrontend: [false],
            confirmDelete: [''],
            tagType: [1]
        });
        this.visibleTags.push({ id: -1 } as Tag);
        this.activeTabId = -1;
    }

    public showInFrontend(tagId: number) {
        return this.formGroupMap[tagId].controls['showInFrontend'].getRawValue();
    }

    public confirmDelete(tagId: number): boolean {
        return this.formGroupMap[tagId].controls['confirmDelete'].getRawValue() === 'verwijder';
    }

    public displayNameFilledIn(tagId: number): boolean {
        return !!this.displayName(tagId) && !!this.displayNameEn(tagId);
    }

    public displayName(tagId: number): string {
        return this.formGroupMap[tagId].controls['displayName'].getRawValue();
    }

    public displayNameEn(tagId: number): string {
        return this.formGroupMap[tagId].controls['displayNameEn'].getRawValue();
    }
}