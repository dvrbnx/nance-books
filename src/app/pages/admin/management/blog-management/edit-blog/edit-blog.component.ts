import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from 'src/app/services/language.service';
import { SupabaseService } from 'src/app/services/supabase.service';
import { ThemeService } from 'src/app/services/theme.service';
import { ToastService } from 'src/app/services/toast.service';
import { ViewPostAdmin } from 'src/app/types/supabase/supabase.types';
import { Editor, Toolbar } from 'ngx-editor';
import { v4 as uuidv4 } from 'uuid';
import { UserService } from 'src/app/services/session.service';
import { firstValueFrom } from 'rxjs';

@Component({
    selector: 'app-edit-blog',
    templateUrl: 'edit-blog.component.html',
    styleUrls: [
        'edit-blog.component.scss'
    ]
})

export class EditBlogComponent implements OnInit, OnDestroy {
    public editor!: Editor;
    public editorEN!: Editor;
    public toolbar: Toolbar = [
        ['bold', 'italic'],
        ['underline', 'strike'],
        ['blockquote'],
        ['ordered_list', 'bullet_list'],
        ['link', 'image'],
        ['align_left', 'align_center', 'align_right', 'align_justify'],
        ['horizontal_rule', 'format_clear']
      ];

    public post!: ViewPostAdmin;
    public theme$ = this.themeService.theme$;
    public language$ = this.languageService.currentLanguage$;
    public blogForm!: FormGroup;

    private uploadedImage?: File = undefined;
    public newImagePreview: string | ArrayBuffer | null = null;
    public fileName?: string = undefined;

    private user: string = '';
    private pronouns: string | undefined | null = '';

    public labels = {
        news: $localize`:@@news:Nieuws`
    }

    public get title(): string {
        return this.blogForm.controls['title'].getRawValue();
    }

    public get title_EN(): string {
        return this.blogForm.controls['title_EN'].getRawValue();
    }

    public get htmlContent(): string {
        return this.blogForm.controls['htmlcontent'].value;
    }

    public get htmlContent_EN(): string {
        return this.blogForm.controls['htmlcontent_EN'].value;
    }

    public get published(): boolean {
        return this.blogForm.controls['published'].getRawValue();
    }

    public get created_at(): string {
        return this.post?.created_at ?? new Date().toLocaleString();
    }

    public get author(): string {
        return this.post?.author ?? this.user;
    }

    public get authorPronouns(): string | undefined | null {
        return this.post?.pronouns ?? this.pronouns;
    }

    constructor(
        private supabaseService: SupabaseService,
        private toastService: ToastService,
        private themeService: ThemeService,
        private languageService: LanguageService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private userService: UserService
    ) { }

    public async ngOnInit(): Promise<void> {
        const path = this.activatedRoute.snapshot.url[1]?.path;

        if (!path) {
            console.error('No path found', this.activatedRoute.snapshot);
            this.toastService.error('Er is iets foutgelopen. Check de logs en contacteer web admin');
        }

        if (path !== '-1') {
            const { data, error } = await this.supabaseService.client
                .from('view_posts_admin')
                .select('*')
                .eq('id', path)
                .single();

            if (error) {
                console.error(error);
                this.toastService.error('Er is iets foutgelopen. Check de logs en contacteer web admin');
            }

            if (!data) {
                console.error('No data returned');
                this.toastService.error('Er is iets foutgelopen. Check de logs en contacteer web admin');
            }

            this.post = data as ViewPostAdmin;
        }
        
        this.editor = new Editor();
        this.editorEN = new Editor();

        this.blogForm = this.fb.group({
            title: [this.post?.title, Validators.required],
            title_EN: [this.post?.title_EN, Validators.required],
            htmlcontent: [this.post?.htmlcontent ?? '', Validators.required],
            htmlcontent_EN: [this.post?.htmlcontent_EN ?? '', Validators.required],
            image: [null],
            published: [this.post?.published ?? false]
        });

        const user = await this.userService.getOwnUser();
        this.user = user?.name ?? 'unknown (contact support)';
        this.pronouns = user?.pronouns;
    }


    public previewImage(event: any): void {
        if (event.target?.files?.length) {
            this.uploadedImage = event.target.files[0];

            if (this.uploadedImage == undefined) {
                return;
            }

            this.fileName = this.uploadedImage.name;
            const reader = new FileReader();
            reader.onload = (e) => {
                this.newImagePreview = e.target?.result ?? null;
            };
            reader.readAsDataURL(this.uploadedImage);
        }
    }

    public async saveBlogImage(): Promise<void> {
        if (this.uploadedImage == undefined) {
            return Promise.resolve();
        }

        const uuid = uuidv4();
        const fileExtension = this.uploadedImage.type.split('/').pop(); 
        const fileName = `${uuid}.${fileExtension}`;
        const { data, error } = await this.supabaseService.client.storage.from('newspostimg')
            .upload(fileName, this.uploadedImage, { contentType: this.uploadedImage.type });

        if (error) {
            console.error(error);
            this.toastService.error('Er is iets foutgelopen, contact IT met logs');
        }    
        else {
            this.toastService.success('Geupload! Klik hieronder nog wel op Opslaan om de afbeelding te linken.');
        }

        this.blogForm.controls['image'].setValue(fileName);
    }

    public async savePost(): Promise<void> {
        const { data, error } = await this.supabaseService.client
            .from('posts')
            .upsert({
                id: this.post?.id,
                title: this.title,
                title_EN: this.title_EN,
                htmlcontent: this.htmlContent,
                htmlcontent_EN: this.htmlContent_EN,
                published: this.published,
                image: this.blogForm.controls['image']?.getRawValue() ?? this.post.image
            });

        if (error) {
            console.log(error);
            this.toastService.error('Er is iets foutgelopen. Contacteer webadmin met logs');
        }
        else {
            this.toastService.success('Saved!');
        }
    }

    public ngOnDestroy(): void {
        this.editor.destroy();
    }
}