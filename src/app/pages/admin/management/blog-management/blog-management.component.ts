import { Component, OnInit, signal, TemplateRef, WritableSignal } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SupabaseService } from 'src/app/services/supabase.service';
import { ThemeService } from 'src/app/services/theme.service';
import { ToastService } from 'src/app/services/toast.service';
import { Post } from 'src/app/types/supabase/supabase.models';

@Component({
    selector: 'app-blog-management',
    templateUrl: 'blog-management.component.html',
    styleUrls: [
        'blog-management.component.scss'
    ]
})

export class BlogManagementComponent implements OnInit {
    public blogPosts: Post[] = []; 
    public theme$ = this.themeService.theme$;

    public blogToDelete: string = '';
    public confirmDeleteInput: string = '';
	closeResult: WritableSignal<string> = signal('');
    
    constructor(
        private supabaseService: SupabaseService,
        private toastService: ToastService,
        private themeService: ThemeService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: NgbModal
    ){}

    public async ngOnInit(): Promise<void> {
        const blogPosts = await this.getAllBlogPosts();
        this.blogPosts.push(...blogPosts);
    }


    public createPost(): void {
        this.router.navigate(['edit', -1], { relativeTo: this.activatedRoute })
    }

    public editPost(post: Post): void {
        this.router.navigate(['edit', post.id], { relativeTo: this.activatedRoute })
    }

    private async getAllBlogPosts(): Promise<Post[]> {
        const { data, error } = await this.supabaseService.client
            .from('posts')
            .select('*')
            .order('created_at', { ascending: false });

        if (error) {
            console.error(error);
            this.toastService.error('Er is iets foutgelopen. Check de logs en contacteer web admin');
        }

        return data as Post[];
    }

	public showDeleteModal(content: TemplateRef<any>, post: Post): void {
        this.blogToDelete = post.title ?? 'ER IS IETS MIS, GA NIET VERDER, CONTACTEER DARLEEN';
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				if (result === 'delete' && this.confirmDeleteInput === 'verwijder') {
                    this.deletePost(post.id);
                }
			},
			(reason) => {
				// this.closeResult.set(`Dismissed ${this.getDismissReason(reason)}`);
			},
		);
	}

    public async deletePost(postId: number): Promise<void> {
        const { data, error } = await this.supabaseService.client
            .from('posts')
            .delete()
            .eq('id', postId);

        if (error) {
            console.log(error);
            this.toastService.error('Er is iets foutgelopen. Contacteer webadmin met logs');
        }
        else {
            this.toastService.success('Deleted! Refresh de pagina om de changes te zien.');
        }
    }
}