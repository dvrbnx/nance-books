import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { firstValueFrom, of } from 'rxjs';
import { LibraryService } from 'src/app/services/library.service';
import { SupabaseService } from 'src/app/services/supabase.service';
import { ThemeService } from 'src/app/services/theme.service';
import { ToastService } from 'src/app/services/toast.service';
import { isbn10Validator } from 'src/app/shared/validators/isbn10.validator';
import { isbn13Validator } from 'src/app/shared/validators/isbn13.validator';
import { State } from 'src/app/types';
import { BookWithRelations } from 'src/app/types/supabase/supabase-extended.models';
import { Book, Tag, Publisher, UpdatePublisher, UpdateBook, InsertPublisher, Author, InsertBook } from 'src/app/types/supabase/supabase.models';

@Component({
    selector: 'app-book-detail-editor',
    templateUrl: './book-detail-edit.component.html',
    styleUrls: ['./book-detail-edit.component.scss']
})
export class BookDetailEditorComponent implements OnInit {
    // UI
    public showEdit: boolean = false;
    public errorMessage: string | null = null;

    public book!: BookWithRelations;

    public breadcrumbs?: any[];

    public image: string | ArrayBuffer | null = null;

    private uploadedImage?: File = undefined;
    public newImagePreview: string | ArrayBuffer | null = null;
    public fileName?: string = undefined;

    public tagOptions!: Tag[];
    public publisherOptions!: (Publisher | InsertPublisher)[];
    public bookForm!: FormGroup;
    public authorOptions!: Author[];
    public newPublisher: string = '';

    public stockCount!: number;
    public errors: string[] = [];
    public maxPublishedYear = new Date().getFullYear() + 10;

    public isViewOnlyInitialValue: boolean = false;
    public isViewOnly: boolean = false;

    public theme$ = this.themeService.theme$;
    public queryParams: Params = {};

    @Input() public showBackArrow: boolean = true;

    get isbn10() {
        return this.bookForm.get('isbn10');
    }

    get isbn13() {
        return this.bookForm.get('isbn13');
    }

    get publishedYear() {
        return this.bookForm.get('publishedYear');
    }

    get bookTitle() {
        return this.bookForm.get('bookTitle');
    }

    get publisher() {
        return this.bookForm.get('publisher');
    }

    constructor(
        private libraryService: LibraryService,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private toastService: ToastService,
        private themeService: ThemeService,
        private supabaseService: SupabaseService
    ) { }

    public ngOnInit(): void {
        this.queryParams = this.route.snapshot.queryParams;
        this.book = this.route.snapshot.data['book'] ?? this.newBook();

        this.stockCount = this.book.copies?.length ?? 0;

        // For change detection
        this.isViewOnlyInitialValue = this.book.copies?.every((copy: any) => copy.state === State.viewOnly) ?? false;
        this.isViewOnly = this.isViewOnlyInitialValue;

        this.tagOptions = this.route.snapshot.data['tags'];
        this.authorOptions = this.route.snapshot.data['authors'];
        // this.breadcrumbs = [
        //     { label: 'Boeken', routerLink: ['../../../'] },
        //     { label: this.book.title, routerLink: ['../'] },
        //     { label: 'Bewerken' }
        // ];
        this.initForm();

        if (this.book.uuid) {
            this.image = this.libraryService.getBookCoverUrl(this.book.uuid);
        }

        this.libraryService.getPublishers().then(response => {
            this.publisherOptions = response;
        });
    }

    private newBook(): InsertBook {
        return {
            description: '',
            enableSync: false,
            isbn10: '',
            isbn13: '',
            pageCount: null,
            publishedYear: null,
            recommended: false,
            subtitle: '',
            title: '',
            uuid: null,
            hidden: false,
            syncedFrom: []
        }
    }

    public initForm(): void {
        const publisher = this.book.publishers?.length
            ? this.book.publishers[0]
            : undefined;
        this.bookForm = this.formBuilder.group({
            bookTitle: [this.book.title, Validators.required],
            bookSubtitle: [this.book.subtitle],
            authors: [this.book.authors],
            publisher: [publisher],
            description: [this.book.description],
            isbn10: [this.book.isbn10, isbn10Validator()],
            isbn13: [this.book.isbn13, isbn13Validator()],
            tags: [this.book.tags],
            publishedYear: [this.book.publishedYear, [Validators.min(0), Validators.max(new Date().getFullYear() + 10)]],
            pageCount: [this.book.pageCount],
            image: [this.book.uuid],
            hidden: [this.book.hidden]
        });
    }

    public async submitBook(): Promise<void> {
        if (this.book.id) {
            await this.updateBook();
        } else {
            await this.createBook();
        }
    }

    public async createBook(): Promise<void> {
        this.errors = [];
        const book: InsertBook = {
            title: this.bookForm.controls['bookTitle'].value,
            subtitle: this.bookForm.controls['bookSubtitle'].value,
            publishedYear: this.bookForm.controls['publishedYear'].value,
            pageCount: this.bookForm.controls['pageCount'].value,
            description: this.bookForm.controls['description'].value,
            isbn10: this.bookForm.controls['isbn10'].value,
            isbn13: this.bookForm.controls['isbn13'].value,
            uuid: this.bookForm.controls['image'].value ?? this.book.uuid,
            hidden: this.bookForm.controls['hidden'].value,
            syncedFrom: []
        };


        const authors = this.bookForm.get('authors')?.value;
        const tags = this.bookForm.get('tags')?.value;
        const publishers = this.bookForm.get('publisher')?.value;
        await this.libraryService.addBook(book, authors, tags, publishers, this.stockCount);
    }

    public async updateBook(): Promise<void> {
        this.errors = [];
        const book: UpdateBook = {
            id: this.book.id,
            title: this.bookForm.controls['bookTitle'].value,
            subtitle: this.bookForm.controls['bookSubtitle'].value,
            publishedYear: this.bookForm.controls['publishedYear'].value,
            pageCount: this.bookForm.controls['pageCount'].value,
            description: this.bookForm.controls['description'].value,
            isbn10: this.bookForm.controls['isbn10'].value,
            isbn13: this.bookForm.controls['isbn13'].value,
            uuid: this.bookForm.controls['image'].value ?? this.book.uuid,
            hidden: this.bookForm.controls['hidden'].value
        };
        
        const authors = this.bookForm.get('authors')?.value;
        const tags = this.bookForm.get('tags')?.value;
        const publishers = this.bookForm.get('publisher')?.value;

        await this.libraryService.updateBook(book, authors, tags, publishers);

        try {
            while (this.stockCount > this.book.copies?.length) {
                await this.addCopy();
            }
            while (this.stockCount < this.book.copies?.length) {
                await this.removeCopy();
            }
        }
        catch (error) {
            console.error('something went wrong', error);
        }

        if (this.isViewOnly !== this.isViewOnlyInitialValue) {
            if (this.isViewOnly) {
                await this.supabaseService.client.from('copies')
                    .update({ state: State.viewOnly })
                    .eq('bookId', this.book.id);
            } else {
                await this.supabaseService.client.from('copies')
                    .update({ state: State.available })
                    .eq('bookId', this.book.id);
            }
        }

        this.toastService.success('De veranderingen werden succesvol opgeslagen!');
    }

    addNewPublisher() {
        const newPublisher = this.newPublisher.trim();
        if (newPublisher && !this.publisherOptions.filter(p => p.name === newPublisher)) {
            this.publisherOptions.push({ name: newPublisher });
            this.bookForm.get('publisher')?.setValue(newPublisher);
        }
        this.newPublisher = '';
    }

    public async addCopy(): Promise<void> {
        await this.libraryService.addCopy(this.book);
        this.book = await this.libraryService.getBook(this.book.id);
    }

    public previewImage(event: any): void {
        if (event.target?.files?.length) {
            this.uploadedImage = event.target.files[0];

            if (this.uploadedImage == undefined) {
                return;
            }

            this.fileName = this.uploadedImage.name;
            const reader = new FileReader();
            reader.onload = (e) => {
                this.newImagePreview = e.target?.result ?? null;
            };
            reader.readAsDataURL(this.uploadedImage);
        }
    }

    public async removeCopy(): Promise<void> {
        const { error } = await this.libraryService.removeCopy(this.book.id);
        if (error) {
            this.errors.push(error);
        }
        this.book = await this.libraryService.getBookAdmin(this.book.id);
    }

    public async saveBookCover(): Promise<void> {
        if (this.uploadedImage == undefined) {
            return Promise.resolve();
        }
        const uuid = await this.libraryService.addImage(this.uploadedImage);
        if (uuid) {
            this.toastService.success('Uploaded!');
        } else {
            this.toastService.error('Error... Probeer opnieuw of stuur the IT crowd');
        }

    }

    public removeTag(id: number): void {
        const currentValue = this.bookForm.controls['tags'].getRawValue();
        this.bookForm.controls['tags'].setValue(currentValue.filter((tag: Tag) => tag.id !== id));
    }

    public removeAuthor(id: number): void {
        const currentValue = this.bookForm.controls['authors'].getRawValue();
        this.bookForm.controls['authors'].setValue(currentValue.filter((author: Author) => author.id !== id));
    }

    public setErrorMessage(message: string): void {
        this.errorMessage = message;
    }
}
