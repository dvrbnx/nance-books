export const stringToHtml = (text: string): string => {
    return text.replace(new RegExp('\n', 'g'), '<br />');
}
