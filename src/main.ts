/// <reference types="@angular/localize" />

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// If language specified then pick this language
let locale = 'nl';
if (window.location.pathname.startsWith('/en')) {
  locale = 'en';
}
else {
  const preferredLanguage = localStorage.getItem('preferredLanguage');
  if (preferredLanguage) {
    locale = preferredLanguage;
    if (preferredLanguage === 'en') {
      location.href = 'en' + window.location.pathname + window.location.search;
    }
  }
}
window.locale = locale;
(window as any).LOCALE_ID = locale;

const translationFile = `/assets/i18n/messages${locale === 'nl' ? '' : `.${locale}`}.json`;

fetch(translationFile)
  .then((response) => response.json())
  .then((translations) => {
    window.$localize = (messageParts: TemplateStringsArray, ...expressions: any[]) => {
      const fullMessage = messageParts[0];
      const match = fullMessage.match(/:@@(.*?):/);
      const key = match ? match[1] : fullMessage;
      const translation = translations.translations[key];
      return translation || key;
    };

    return platformBrowserDynamic().bootstrapModule(AppModule);
  })
  .catch((err) => console.error(err));
