# Project Madrigal (front-end/nance-books)
![Project madrigal logo](src/assets/logo.svg){width=100 height=100px} A project to run a library: adding books, viewing them, and lending them out.

For info on the back-end/api, click [here](https://gitlab.com/project-madrigal/project-madrigal-api)!


## Running locally/development
Please check out the [wiki](https://gitlab.com/project-madrigal/project-madrigal/-/wikis/home) for more info.


