#!/bin/bash

if [[ $VERCEL_GIT_COMMIT_REF == "main"  ]] ; then 
  npm run build:main
else
  npm run build:development
fi